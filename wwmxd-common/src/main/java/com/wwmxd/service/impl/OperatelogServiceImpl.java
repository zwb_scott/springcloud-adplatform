package com.wwmxd.service.impl;

import com.wwmxd.entity.Operatelog;
import com.wwmxd.dao.OperatelogDao;
import com.wwmxd.service.OperatelogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WWMXD
 * @since 2018-03-02 16:25:03
 */
public class OperatelogServiceImpl extends ServiceImpl<OperatelogDao, Operatelog> implements OperatelogService  {
	
}
