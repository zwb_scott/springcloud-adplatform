package cn.dota2info.mq.Controller;

import cn.dota2info.mq.event.RocketmqEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * 模拟消费者
 */
@Component
public class ConsumerDemo {
    @EventListener(condition = "#event.msgs[0].topic=='TopicTest1' && #event.msgs[0].tags=='TagA'")
    public void rocketmqMsgListen(RocketmqEvent event) {
//      DefaultMQPushConsumer consumer = event.getConsumer();
        try {
            System.out.println("com.guosen.client.controller.consumerDemo监听到一个消息达到：" + new String(event.getMsgs().get(0).getBody()));
            // TODO 进行业务处理
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

