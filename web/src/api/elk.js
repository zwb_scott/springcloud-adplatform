import request from '@/assets/util/request.js'
import {ELKURL, MENUID} from "../assets/constants/constant-common";

export function addBook(formData) {
  return request({
    url: ELKURL+MENUID.addBook,
    method: 'post',
    data: formData
  })
}
export function editBook(formData) {
  return request({
    url: ELKURL+MENUID.updateBook,
    method: 'post',
    data: formData
  })
}
